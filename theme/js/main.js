app = {};

$(function () {
	app.init();
	app.scrollWidth();
	//app.pagination();
	app.shareArticle();
	if ($(".article").length) {
		app.stickySidebar();
		app.videoAdaptation();
	}
	if($("[data-twitter]").length) app.stickyTwit();
	app.slider();
	app.blurImg();
	app.bodyHeight();
	app.anchorLinks();
	app.anchorss();
	if ($("[data-body=main]").length || $("[data-body=news]").length) app.loadAdd();
	if ($("[data-body=projects]").length) app.loadAddProjects();
	if ($("[data-body=search-tags]").length) app.loadAddTags();
	if ($("[data-body=main]").length){
		 app.tabletMain();
		 app.sidebarBanner();
	}
	app.parser();
	app.formSuccess();
	app.mobileMenu();
	app.selectionDevice();
	app.select();
});

app.init = function() {

};

app.selectionDevice = function() {
	var $window = $(window);

	/*$window.on("deviceDesc", function(){
	    console.log("deviceDesc");
	});
	$window.on("deviceTablet", function(){
	    console.log("deviceTablet");
	});
	$window.on("deviceMobile", function(){
	    console.log("deviceMobile");
	});*/

	var flagSwitch = "desc",
		scrollWidth = app.scrollWidth(),
		selectionDeviceFuncFirst = function(){
		    if ($(window).width()>=1300 - scrollWidth){
		        flagSwitch = "desc";
		        $window.trigger("deviceDesc");
		    }else {
		        if ($(window).width()>=768 - scrollWidth) {
		            flagSwitch = "tablet";
		            $window.trigger("deviceTablet");
		        } else{
		            flagSwitch = "mobile";
		            $window.trigger("deviceMobile");
		        }
		    }
		},
		selectionDeviceFunc = function(){
		    if ($(window).width()>=1300 - scrollWidth){
		    	if (!(flagSwitch=="desc")){
					flagSwitch = "desc";
					$window.trigger("deviceDesc");
					scrollWidth = app.scrollWidth();
		    	}
		    }else {
		        if ($(window).width()>=768 - scrollWidth) {
					if (!(flagSwitch=="tablet")){
		            	flagSwitch = "tablet";
			            $window.trigger("deviceTablet");
			            scrollWidth = app.scrollWidth();
					}
		        } else{
					if (!(flagSwitch=="mobile")){
						flagSwitch = "mobile";
						$window.trigger("deviceMobile");
						scrollWidth = app.scrollWidth();
					}   
		        }
		    }
		};
	//Первый запуск	
	selectionDeviceFuncFirst();
	//При ресайзе
    $(window).resize(function() {
		selectionDeviceFunc();
    });
};

app.select = function() {
	var $select = $("[data-select]"),
		scrollWidth = app.scrollWidth();

	if ($(window).width()>=768 - scrollWidth) $select.selectric();

	$(window).on("deviceDesc deviceTablet ", function(){
		$select.selectric('init');
    });

	$(window).on("deviceMobile", function(){
		$select.selectric('destroy');
	});
};

app.videoAdaptation = function() {
	var $iframe = $("iframe");
	$iframe.wrap("<div class='video__wrap'></div>");
};
//ШЕРИНГ
app.shareArticle = function() {
	Share = {
		vkontakte: function(purl, ptitle, pimg, text) {
			url  = 'http://vk.com/share.php?';
			url += 'url='          + encodeURIComponent(purl);
			url += '&title='       + encodeURIComponent(ptitle);
			url += '&description=' + encodeURIComponent(text);
			url += '&image='       + encodeURIComponent(pimg);
			url += '&noparse=true';
			Share.popup(url);
		},
		odnoklassniki: function(purl, text) {
			url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
			url += '&st.comments=' + encodeURIComponent(text);
			url += '&st._surl='    + encodeURIComponent(purl);
			Share.popup(url);
		},
		facebook: function(purl, ptitle, pimg, text) {
			url  = 'https://www.facebook.com/sharer/sharer.php?';
			url += 'u='       + encodeURIComponent(purl);
			url += '&t='       + encodeURIComponent(pimg);
			//url += '&t='     + encodeURIComponent(ptitle);
			//url += '&p[summary]='   + encodeURIComponent(text);
			//url += '&p[images][0]=' + encodeURIComponent(pimg);
			Share.popup(url);
		},
		twitter: function(purl, ptitle) {
			url  = 'http://twitter.com/share?';
			url += 'text='      + encodeURIComponent(ptitle);
			url += '&url='      + encodeURIComponent(purl);
			url += '&counturl=' + encodeURIComponent(purl);
			Share.popup(url);
		},
		mailru: function(purl, ptitle, pimg, text) {
			url  = 'http://connect.mail.ru/share?';
			url += 'url='          + encodeURIComponent(purl);
			url += '&title='       + encodeURIComponent(ptitle);
			url += '&description=' + encodeURIComponent(text);
			url += '&imageurl='    + encodeURIComponent(pimg);
			Share.popup(url)
		},
		telegram: function(purl, ptitle, pimg, text) {
			url  = 'https://t.me/share/url?';
			url += 'url='          + encodeURIComponent(purl);
			Share.popup(url)
		},
		popup: function(url) {
			console.log(url);
			window.open(url,'','toolbar=0,status=0,width=626,height=436');
		}
	};
};

app.sidebarBanner = function() {
	var $banner = $("[data-sidebar-banner]"),
		$sidebar = $("[data-sidebar]");
		
	if (!($banner.length)){
		$sidebar.addClass("_none-banner");
	}
};
//ШИРИНА СКРОЛЛА
app.scrollWidth = function() {
	// создадим элемент с прокруткой
	var div = document.createElement('div');

	div.style.overflowY = 'scroll';
	div.style.width = '50px';
	div.style.height = '50px';
	div.style.visibility = 'hidden';

	document.body.appendChild(div);
	var scrollWidth = div.offsetWidth - div.clientWidth;
	document.body.removeChild(div);
	return scrollWidth;
};
//МОБИЛЬНОЕ МЕНЮ
app.mobileMenu = function () {
	var $btn = $("[data-menu-btn]"),
		$body = $("[data-body]"),
		$footer = $("[data-footer]"),
		$header = $("[data-header]"),
		open = function(){
			$btn.addClass("_active");
			$wrapperMob.addClass("_active");
			$menu.css({
				"min-height" : $(window).height()-$header.outerHeight()-$currency.outerHeight()-$search.outerHeight()
			});
			$wrapperMob.stop().animate({left: "0"}, 300);
            setTimeout(function () {
				$body.hide();
				$footer.hide();
            }, 300);
		},
		close = function(){
			$btn.removeClass("_active");
			$body.show();
			$footer.show();
			$wrapperMob.stop().animate({left: "100%"}, 300);
            setTimeout(function () {
				$wrapperMob.removeClass("_active");
            }, 300);
			
		};

    var $wrapperMob = $("[data-menu-wrapper=mobile]"),
    	$wrapperDesc = $("[data-menu-wrapper=desc]"),
    	$menu = $("[data-menu]"),
    	$currency = $("[data-currency-wrap]"),
    	$search = $("[data-search]");

	$btn.on("click",function(e){
		e.preventDefault();
		if ($btn.hasClass("_active")){
			close();
		}else{
			open();
		}
	});

    $(window).on("deviceTablet deviceDesc", function(){
		$btn.before($currency);
		$btn.before($search);
		$wrapperDesc.append($menu);
		close();
		$menu.css({
			"min-height" : "auto"
		});
    });

	$(window).on("deviceMobile", function(){
	    $wrapperMob.append($currency);
    	$wrapperMob.append($search);
    	$wrapperMob.append($menu);
	});
};
//ОТОБРАЖЕНИЕ БЛОКА ПОДПИСКИ И САЙДБАРА В ПЛАНШЕТНОЙ И ДЕСКТОПНОЙ ВЕРСИЯХ
app.tabletMain = function () {
	var $subscription = $("[data-subscription]"),
		$sidebar = $("[data-sidebar]");

    $(window).on("deviceDesc", function(){
		$sidebar.prepend($subscription);
    });

	$(window).on("deviceMobile deviceTablet", function(){
		$sidebar.before($subscription);
	});
};
//ПОПАП БЛАГОДАРНОСТИ
app.formSuccess = function(){
	var $confirm = $("[data-src='#confirm']"),
		$already = $("[data-src='#already']"),
		mailingOfLetters = function(email){
			$.ajax({
			  type: 'POST',
			  url: "http://chainmedia.ru/assets/getresponse-api-php/addContact.php",
			  success: onAjaxSuccess,
			  data: "email="+email
			});
	        function onAjaxSuccess(data) {
	            if(data){ 
            		//console.log(data);
            		test = $.parseJSON(data);
            		//console.log(test.code);
            		if (!(test.code==undefined)){
            			if (test.code==1008){
            				//email уже в списке рассылки
            				$already.click();
            			}
            			if (test.code==1002){
            				//email в чёрном списке
            			}
            		}else{
            			//В списке нет email. Выслали письмо подтверждение
            			$confirm.click();
            		}
	            }       
	        }
	        return false;
		};

    $(document).on('af_complete', function(event, response) {
    	var $input = $("[data-subscriber-email]"),
    		emailSubscriber = $input.val();

        if (response.success){
			mailingOfLetters(emailSubscriber);
	        return false;
        }
    });
};
//ПАРСИМ КРИПТОВАЛЮТУ
app.parser = function () {
	var $item = $("[data-currency]"),
		flagFirst = true,
		parserFunc = function(){
			$.ajax({
			  type: 'POST',
			  url: "https://min-api.cryptocompare.com/data/pricemultifull?fsyms=BTC,BCH,ETH&tsyms=USD",
			  success: onAjaxSuccess
			});
	        function onAjaxSuccess(data) {
	            if(data){ 
	            	insetVal("btc",data.DISPLAY.BTC.USD.PRICE,data.DISPLAY.BTC.USD.CHANGEPCT24HOUR);
	            	insetVal("bch",data.DISPLAY.BCH.USD.PRICE,data.DISPLAY.BCH.USD.CHANGEPCT24HOUR);
	            	insetVal("eth",data.DISPLAY.ETH.USD.PRICE,data.DISPLAY.ETH.USD.CHANGEPCT24HOUR);
	            }       
	        }
	        return false;
		},

		insetVal = function(id, val, pct){

			var $itemSingle = $item.filter("[data-currency="+id+"]"),
				$val = $itemSingle.find("[data-currency-val="+id+"]"),
				$odds = $itemSingle.find("[data-currency-odds="+id+"]");
			$val.text(val);

			$odds.removeClass("_minus _plus");
			if (!(pct==0)){
				if (pct<0){
					$odds.text(pct+" %");
					$odds.addClass("_minus");
				} else{
					$odds.text(pct+" %");
					$odds.addClass("_plus");
				}
			}
		};
	parserFunc();
	setInterval (function(){
		parserFunc();
	}, 10000); 
};
//ЗАГРУЗКА СТАТЕЙ
app.loadAdd = function(){
    var $btn = $("[data-btn-load]"),
        $form = $("[data-form]"),
        $content = $("[data-content]"),
        $allCount = $("[data-all-count-news]"),
        $banners = $("[data-banner]"),
        allCountValue = Number($allCount.val());
        //firstValue = Number($count.val());
    //Кол-во новостей при загрузке
	var $count = $("[data-count-news]"),
		$news = $("[data-new-id]");
	$count.val($news.length);

	if (($news.length + $banners.length) < 6 ){
		$btn.hide();
	}
    $btn.on('click',function(e){
        e.preventDefault();
        $form.submit();
    });

    $form.submit(function() {
        $btn.addClass("_active");
        $.post(
            $(this).attr('action'),
            $(this).serialize(),
            onAjaxSuccess
        );  
        function onAjaxSuccess(data) {
            if(data){ 
                $content.append(data);
			    //Кол-во новостей
				var $count = $("[data-count-news]"),
					$news = $("[data-new-id]");
				$count.val($news.length);
				var $countLoad = $("[data-count-load]"),
					$countLoadVal = Number($countLoad.val()) + 1;
				$countLoad.val($countLoadVal);
				if ( Number($count.val()) == Number($allCount.val())){
					$btn.hide();
				}
                $btn.removeClass("_active");
            }       
        }
        return false;
    });
};
//Загрузка проектов
app.loadAddProjects = function(){
    var $btn = $("[data-btn-load]"),
        $form = $("[data-form]"),
        $inputAllCount = $("[data-all-count-news]"),
		$allCount = Number($inputAllCount.val()),
		$content = $("[data-content]"),
		$wrapper = $("[data-content-wrapper]"),
		$find = $("[data-find]"),
		$filter = $("[data-filter]"),
		funcCount = function(){
			var $project = $("[data-project]"),
				count = $project.length,
				$inputCount = $("[data-count-news]");

			$inputCount.val(count);
			return count;
		},
		funcBtnClick = function(e){
			e.preventDefault();
			var $form = $("[data-form]");
			$form.submit();
		},
		funcFormSubmit = function(){
			var $form = $("[data-form]"),
				$btn = $("[data-btn-load]"),
				$inputAllCount = $("[data-all-count-news]"),
				$allCount = Number($inputAllCount.val());

			$form.submit(function() {
		        $btn.addClass("_active");
		        $.post(
		            $(this).attr('action'),
		            $(this).serialize(),
		            onAjaxSuccess
		        );  
		        function onAjaxSuccess(data) {
		            if(data){
		            	var $content = $("[data-content]");
						$content.append(data); 
		            } 
		            $btn.removeClass("_active");
	                if ((funcCount()>=$allCount)){
						$btn.hide();
					}else{
						$btn.show();
					}      
		        }
		        return false;
	        });
		},
		funcFilterSubmit = function(){
			var $filter = $("[data-filter]");

			$filter.submit(function() {
		        $.post(
		            $(this).attr('action'),
		            $(this).serialize(),
		            onAjaxSuccess
		        );  
		        function onAjaxSuccess(data) {
		            if(data){
		        		$wrapper.empty();
		        		$wrapper.append(data);
		            }   
					var $form = $("[data-form]"),
				        $inputAllCount = $("[data-all-count-news]"),
						$allCount = Number($inputAllCount.val()),
						$content = $("[data-content]"),
						$btn = $("[data-btn-load]");

					if ((funcCount()>=$allCount)){
						$btn.hide();
					}else{
						$btn.show();
					}

					$btn.unbind('click');
					$btn.bind('click',function(e){
						funcBtnClick(e);
					});	
					funcFormSubmit();    
		        }
		        return false;
	        });
		};

	//Показываю кнопку, если нужно
    if ((funcCount()<$allCount)){
    	$btn.show();
    }
    //Обработчик для кнпоки загрузить ещё
	$btn.bind('click',function(e){
		funcBtnClick(e);
	});
	funcFormSubmit();

	//Обработчик для кнпоки найти
	$find.on('click',function(e){
		e.preventDefault();
		$filter.submit();
	});
	funcFilterSubmit();
};
//АНИМАЦИЯ НА СТРАНИЦЕ ТЕГОВ
app.loadAddTags = function () {
	var $btn = $("[data-btn-load-tags]");
	if ($btn.length) {
		pdoPage.callbacks['before'] = function(config) {
		    $btn.addClass("_active");
		};
		pdoPage.callbacks['after'] = function(config, response) {
		    $btn.removeClass("_active");
		};
	}
};
//АНКОРЫ
app.anchorss =  function(){
    var $anchor = $('[data-anchors]'),
	    $anchorPlace = $('[data-anchors-place]'),
	    pageHref = location;

	pageHref = pageHref.toString();

   $anchor.click(function (){
        var id = $(this).data('anchors'),
            href = pageHref+"#"+id;

        $('html, body').animate({
            scrollTop: $anchorPlace.filter('[data-anchors-place='+id+']').offset().top
        }, 500);

        try {
        history.pushState(null, null, href);
            return;
        } catch(e) {}
        location.hash = '#' + href;

        return false;
    });
};
//СОДЕРЖАНИЕ НА ШАБЛОНЕ "ДЛЯ НОВЧИКОВ
app.anchorLinks = function () {
	var $block = $("[data-anchor-block]"),
		$body = $("[data-anchor-body]"),
        pageHref = location;

    pageHref = pageHref.toString();

	var number = pageHref.indexOf("#");

	//Если в url есть анкор
	if (!(number== -1)){
		pageHref = pageHref.substring(0, number);
	}
	if ($block.length){
		var $article = $("[data-article]"),
			$h = $article.find("h2, h3"),
			$h2 = $h.filter("h2"),
			$h3 = $h.filter("h3");

		$h2.attr("data-h","h2");
		$h3.attr("data-h","h3");

		var indexH2 = 0;
			indexH3 = 1;
		//ПЕРЕБИРАЕМ ЗАГОЛОВКИ И СОЗДАЁМ ОГЛАВЛЕНИЕ
		$h.each(function(index){
			var attr = $(this).data("h"),
				text = $(this).text();
			if ( attr == "h2"){	
				indexH2 = index;
				$(this).prepend("<a href='#' name='h2-"+indexH2+"' data-anchors-place='h2-"+indexH2+"'></a>")
				$body.append("<div class='anchor-links__item'><span class='anchor-links__caption' data-anchor-caption="+index+"><span class='anchor-links__caption-icon'><span class='anchor-links__caption-icon-up'></span></span>"+text+"</span><div class='anchor-links__wrapper' data-anchor-wrapper="+index+"></div><div/>");
						
			} else{
				$(this).prepend("<a href='#' name='h3-"+indexH3+"' data-anchors-place='h3-"+indexH3+"'></a>")
				var $wrapper = $("[data-anchor-wrapper="+indexH2+"]");
				$wrapper.append("<div class='anchor-links__link-wrap'><a href='"+pageHref+"#h3-"+indexH3+"' class='anchor-links__link' data-anchors=h3-"+indexH3+">"+text+"</a></div>");
				indexH3++;
			}
		});
		//СЛУЧАЙ, КОГДА У ЗАГОЛОВКА H2 НЕТ ВНУТРЕННИХ ЗАГОЛОВКОВ H3
		var $wrapper = $("[data-anchor-wrapper]");
		$wrapper.each(function(index){
			if ($(this).is(':empty')){
				var id = $(this).data("anchor-wrapper"),
					$captionSingle = $("[data-anchor-caption="+id+"]"),
					text = $captionSingle.text();

				$captionSingle.addClass("_empty");
				$captionSingle.replaceWith("<a href='"+pageHref+"#h2-"+id+"'' class='anchor-links__caption _empty' data-anchors='h2-"+id+"'>"+text+"</a>");
			}
		});
		//СПОЙЛЕР ЗАГОЛОВКОВ
		var $caption = $("[data-anchor-caption]"),
			$wrapper = $("[data-anchor-wrapper]");

		$wrapper.hide();
		$caption.on("click", function(){
			$(this).toggleClass("_active").next().stop().slideToggle();
		});
	}
};
//МИНИМАЛЬНАЯ ВЫСОТА СТРАНИЦЫ
app.bodyHeight = function () {
	var $header = $("[data-header]"),
		$footer =$("[data-footer]"),
		$body =$("[data-body]"),
		heightHeader = $header.outerHeight(true),
		heightFooter = $footer.outerHeight(true),
		heightWin = $(window).height(),
		paddingTop = $body.css("padding-top"),
		paddingBottom = $body.css("padding-bottom");

	paddingTop = Number(paddingTop.replace(/[^0-9]/g,''));
	paddingBottom = Number(paddingBottom.replace(/[^0-9]/g,''));
	var heightBody = heightWin - heightHeader - heightFooter - paddingTop - paddingBottom

	$("[data-body]").css({
		"min-height": heightBody
	});
};

app.slider = function () {
	var $slider = $("[data-slider]"),
		$btnPrev = $("[data-slider-btn-prev]"),
		$btnNext = $("[data-slider-btn-next]"),
		$slides = $slider.find("[data-slide]"),
		count = $slides.length,
		$similar = $("[data-similar]"),
		sliderBtn = function(prev,next){
			if (prev.hasClass("swiper-button-disabled") && next.hasClass("swiper-button-disabled")){
				prev.hide();
				next.hide();
			} else {
				prev.show();
				next.show();
			}
		};
 
	if (!(count == 0)){
		var	mySwiper = new Swiper($slider, {
				speed: 400,
				spaceBetween: 100,
				slidesPerView: 3,
				nextButton: $btnNext,
				prevButton: $btnPrev,
				spaceBetween: 27,
				preventClicks: false
			});
		sliderBtn($btnPrev,$btnNext);
	}
	if (!(count == 0)){
		$(window).on("deviceDesc", function(){
			mySwiper.destroy(false,true);
			mySwiper = new Swiper($slider, {
				speed: 400,
				spaceBetween: 100,
				slidesPerView: 3,
				nextButton: $btnNext,
				prevButton: $btnPrev,
				spaceBetween: 27,
				preventClicks: false
			});
			sliderBtn($btnPrev,$btnNext);
		});

		$(window).on("deviceTablet", function(){
			mySwiper.destroy(false,true);
			mySwiper = new Swiper($slider, {
				speed: 400,
				spaceBetween: 100,
				slidesPerView: 2,
				nextButton: $btnNext,
				prevButton: $btnPrev,
				spaceBetween: 27,
				preventClicks: false
			});
			sliderBtn($btnPrev,$btnNext);
		});

		$(window).on("deviceMobile", function(){
			mySwiper.destroy(false,true);
			mySwiper = new Swiper($slider, {
				speed: 400,
				spaceBetween: 100,
				slidesPerView: 1,
				nextButton: $btnNext,
				prevButton: $btnPrev,
				spaceBetween: 27,
				preventClicks: false
			}); 
			sliderBtn($btnPrev,$btnNext);
		});
	}else{
		$similar.hide();
	}

 	var $text = $(".cell__text");

 	$text.on("click", function(){
 		console.log("клик");
 	});

};
//БЛЮР ГЛАВНОЙ КАРТИНКИ СТАТЬИ
app.blurImg = function () {
	if (($(".article").length)  && ($("[data-main-img]").length) ){
		var $img = $("[data-main-img]"),
			top = $img.offset().top;
			height = $img.height(),
			propHeight = height / 5;

		$(window).scroll(function() {
			var top = $img.offset().top;
				height = $img.height(),
				propHeight = height / 5;

			if ( $(window).scrollTop() < top){
				$img.removeClass();
			}
			if ( ($(window).scrollTop() >= top) && ($(window).scrollTop() < top + propHeight) ){
				$img.removeClass();
				$img.addClass("_blur-1");
			}
			if ( ($(window).scrollTop() >= top + propHeight) && ($(window).scrollTop() < top + 2*propHeight) ){
				$img.removeClass();
				$img.addClass("_blur-2");
			}
			if ( ($(window).scrollTop() >= top + 2*propHeight) && ($(window).scrollTop() < top + 3*propHeight) ){
				$img.removeClass();
				$img.addClass("_blur-3");
			}
			if ( ($(window).scrollTop() >= top + 3*propHeight) && ($(window).scrollTop() < top + 4*propHeight) ){
				$img.removeClass();
				$img.addClass("_blur-4");
			}
			if ($(window).scrollTop() >= top + 4*propHeight){
				$img.removeClass();
				$img.addClass("_blur-5");
			}
		});
	}
};
//ПРИЛИПАЮЩИЙ САЙДБАР
app.stickySidebar = function () {
	var $sidebar = $("[data-sidebar]");
		$sidebar.css({
			"height": "auto"
		});

	var $content = $("[data-content]"),
		$body = $(".body"),
		heightBody = $body.height(),
		$oldScrollTop = $(window).scrollTop(),
		direction = "up",
		leftSidebar = $sidebar.position().left,
		sticky = function(){
		//SIDEBAR
		var top = $sidebar.offset().top,
			height = $sidebar.outerHeight(),
			left = $content.width() - $sidebar.width() + $content.offset().left;
			width = $sidebar.width(),
			heightWin = $(window).height();
		//CONTENT
		var topContent = $content.offset().top,
			heightContent = $content.outerHeight(),
			paddingTop = $content.css("padding-top");
			paddingTop = Number(paddingTop.replace(/[^0-9]/g,''));
		var  topPosition = top - topContent;
			$sidebar.css({"margin-bottom":"0px"});
		//НАПРАВЛЕНИЕ
		var $newScrollTop = $(window).scrollTop();
			if ($newScrollTop > $oldScrollTop){
				direction = "down";
			}else {
				direction = "up";
			}
			$oldScrollTop = $newScrollTop;

			//ДО КОНТЕНТА
			if ($(window).scrollTop() < topContent + paddingTop){
				$sidebar.css({
					position: "static",
					top: "auto",
					bottom: "auto",
					left: "auto",
					right: "auto",
					width: width
				});
			//В ОБЛАСТИ КОНТЕНТА	
			} else {
				//ЕСЛИ ЭКРАН БОЛЬШЕ САЙДБАРА
				if ($(window).height() > height){
					if ( ($(window).scrollTop() >= topContent + paddingTop) && ($(window).scrollTop() <= topContent + heightContent - height) ){
						//СКРОЛ ВНИЗ
						$sidebar.css({
							position: "fixed",
							top: "0",
							bottom: "auto",
							left: left,
							right: "auto",
							width: width
						});
					//ПОСЛЕ КОНТЕНТА
					} else {
						$sidebar.css({
							position: "absolute",
							top: "auto",
							bottom: "0",
							left: "auto",
							right: "0",
							width: width
						});
					}
				}else{
					if ( ($(window).scrollTop() >= topContent + paddingTop) && ($(window).scrollTop() <= topContent + heightContent - heightWin) ){
						//СКРОЛ ВНИЗ
						if (direction=="down"){
							if ( $(window).scrollTop() >= top + height - heightWin){
								$sidebar.css({
									position: "fixed",
									top: "auto",
									bottom: "0",
									left: left,
									right: "auto",
									width: width
								});
							} else {
								$sidebar.css({
									position: "absolute",
									top: topPosition,
									bottom: "auto",
									left: "auto",
									right: "0",
									width: width
								});
							}
						}
						//СКРОЛ ВВЕРХ
						if (direction=="up"){
							if ($(window).scrollTop() > top){
								$sidebar.css({
									position: "absolute",
									top: topPosition,
									bottom: "auto",
									left: "auto",
									right: "0",
									width: width
								});
							} else{
								$sidebar.css({
									position: "fixed",
									top: "0",
									bottom: "auto",
									left: left,
									right: "auto",
									width: width
								});
							}
						}
					//ПОСЛЕ КОНТЕНТА
					} else {
						$sidebar.css({
							position: "absolute",
							top: "auto",
							bottom: "0",
							left: "auto",
							right: "0",
							width: width
						});
					}
				}
			}
		};

	var $sidebar = $("[data-sidebar]"),
		$content = $("[data-content]"),
		height = $sidebar.outerHeight(),
		heightContent = $content.outerHeight(),
		paddingTop = $content.css("padding-top");

		paddingTop = Number(paddingTop.replace(/[^0-9]/g,''));

		heightContent = heightContent - Number(paddingTop);


	// При первом запуске
	if ((height < heightContent -20) && ($(window).width()>=1300)){
		sticky();
	}
	//При скроле
	$(window).scroll(function() {
		if ((height < heightContent -20) && ($(window).width()>=1300)){
			sticky();
		}
	});
	//При ресайзе
	$( window ).resize(function(){
		if ((height < heightContent -20) && ($(window).width()>=1300)){
			sticky();
		}
	});
};
app.stickyTwit = function () {
	var $twitter = $("[data-twitter]"),
		$twitterDesc = $("[data-twitter-desc]"),
		twit = function(){
			$twitter.each(function() {
				var	$singleTwitter =  $(this),
					id = $singleTwitter.data("twitter"),
					$singleDesc = $singleTwitter.find("[data-twitter-desc="+id+"]"),
					top = $singleTwitter.offset().top,
					height = $singleTwitter.outerHeight(true),
					heightDesc = $singleDesc.outerHeight();	

				if( ($(window).scrollTop() >= $singleTwitter.offset().top) && ($(window).scrollTop() <= $singleTwitter.offset().top + height - heightDesc )  ) {
					$singleDesc.css({
						position: "fixed",
						top: "0",
						bottom: "auto",
						width: "230px"
					});
				}else{
					if( ($(window).scrollTop() > $singleTwitter.offset().top + height - heightDesc )) {
						$singleDesc.css({
							position: "absolute",
							top: "auto",
							bottom: "0"
						});
					}else{
						$singleDesc.css({
							position: "static",
							top: "auto",
							bottom: "auto"
						});
					}
				}
			});
		};

	//При первом запуске	
	var $twitter = $("[data-twitter]"),
		$desc = $("[data-twitter-desc]"),
		$twit = $("[data-twitter-twit]");

	var top = $twitter.offset().top,
		height = $twitter.outerHeight(),
		heightDesc = $desc.outerHeight(),
		left = $twitter.offset().left,
		width = $desc.width();

	$(window).on("deviceDesc deviceTablet", function(){
	    twit();
	});

	$(window).on("deviceMobile", function(){
		$twitterDesc.css({
			"position": "static",
			"width": "auto"
		});
	});

	$(window).scroll(function() {
	    if ($(window).width()>=768){
				twit();
	    }else {
			$twitterDesc.css({
				"position": "static",
				"width": "auto"
			});
	    }
	});

};
